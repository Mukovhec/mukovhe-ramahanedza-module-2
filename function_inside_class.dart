class App {
  String? name;
  String? sector;
  String? developer;
  int? year;

  void printAppInformation() {
    name = name?.toUpperCase();
    print("Name of the app is $name");
    print("The sector of the app is $sector");
    print("The developer of the app is $developer");
    print("The app was developed in $year");
  }
}

void main(List<String> args) {
  var shyft = new App();
  shyft.name = "shyft";
  shyft.sector = "Online Banking";
  shyft.developer = "Standard Bank";
  shyft.year = 2021;

  shyft.printAppInformation();
}
